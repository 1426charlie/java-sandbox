/**
 * examples of map, filter, reduce
 */

import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import java.io.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StreamUtils {
    Logger log = LoggerFactory.getLogger(StreamUtils.class);

    // int adder = (a, b) -> a + b;
    int sum(List<Integer> list) {
        return list.stream().reduce(0, (a, b) -> a + b);
    }

    int prod(List<Integer> list) {
        return list.stream().reduce(1, (a, b) -> a * b);
    }

    List<Integer> mult(List<Integer> list, int a) {
        // this should work, but it doesn't...
        // return list.stream().mapToInt(n -> n * a).collect();

        List<Integer> nlist = new ArrayList<>(list.size());

        for (int i = 0; i < list.size(); i++) {
            nlist.add( list.get(i) * a);
        }

        return nlist;
    }

    List<Integer> range(int from, int to) {
        int sz = to - from + 1;
        List<Integer> list = new ArrayList<>(sz);

        for (int i = from; i <= to; i++) {
            list.add(i);
        }

        return list;
    }

    public static void main(String[] args) {
        Logger log = LoggerFactory.getLogger(StreamUtils.class);

        final StreamUtils utils = new StreamUtils();
        final List<Integer> list = utils.range(1, 5);

        log.info(String.format("sum : %s from %s\n", utils.sum(list), list));
        log.info(String.format("prod: %s from %s\n", utils.prod(list), list));
        log.info(String.format("mult: %s from %s * %d\n", utils.mult(list, 3), list, 3));
    }
}
