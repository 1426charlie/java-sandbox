/**
 * demonstrates how to protect instanciation against a default contructor while still 
 * enabling extension
 */

import java.util.*;
import java.io.*;

public abstract class A implements Runnable {
    Console log = System.console();

    protected String name;

    private A() {
        log.printf("this is a private class A\n");
    }

    public A(String name) {
        this.name = name;
    }

    // if this were static, it could not be overwritten
    public String tester(String param) {
        return param.toUpperCase();
    }
}

