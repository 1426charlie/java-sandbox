/**
 * @created 2018-12-17 15:07:34 darryl.west
 *
 * ssl ref: https://stackoverflow.com/questions/1828775/how-to-handle-invalid-ssl-certificates-with-apache-httpclient
 */

import java.io.*;
import java.util.*;
import java.net.*;
import javax.net.ssl.*;
import java.security.*;
import java.security.cert.*;

public class HttpsClient {
    private static String apikey = "d5cc698c-15e3-fffc-cb5f-3dbbc491cbf3";

    public void getAPI(String host) throws Exception {
        String endpoint = "api/v1";

        URL url = new URL(String.format("%s/%s", host, endpoint));

        HttpsURLConnection conn = (HttpsURLConnection)url.openConnection();
        // HttpURLConnection conn = (HttpURLConnection)url.openConnection();

        conn.setHostnameVerifier(new HostnameVerifier() {
            @Override
	        public boolean verify(String hostname, SSLSession sslSession) {
	            return true;
	        }
        });

        conn.setRequestMethod("GET");

        conn.setRequestProperty("X-API-Key", apikey);
        conn.setRequestProperty("User-Agent", "java-client");
        conn.setRequestProperty("Accept", "application/json");
        conn.setRequestProperty("Content-Type", "application/json");

        int code = conn.getResponseCode();

        log("request to url: %s", url);
        log("response code : %d", code);

        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        StringBuilder response = new StringBuilder();

        while ((line = in.readLine()) != null) {
            response.append(line);
        }

        in.close();

        log(response.toString());
    }

    void log(String format, Object... args) {
        System.out.printf("%d %s\n", System.currentTimeMillis(), String.format(format, args));
    }

    private static class DefaultTrustManager implements X509TrustManager {
        @Override
        public void checkClientTrusted(X509Certificate[] xc, String str) throws CertificateException {}

        @Override
        public void checkServerTrusted(X509Certificate[] xc, String str) throws CertificateException {}

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }

    private static void initSSL() throws Exception {
        SSLContext ctx = SSLContext.getInstance("TLS");
        X509TrustManager tm = new DefaultTrustManager();
        ctx.init(new KeyManager[0], new TrustManager[] { tm }, new SecureRandom());
        SSLContext.setDefault(ctx);
    }

    public static void main(String... args) throws Exception {
        initSSL();

        String host = "https://35.233.219.141";
        // String host = "https://localhost";

        new HttpsClient().getAPI(host);
    }
}
