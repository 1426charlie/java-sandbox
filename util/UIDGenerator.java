/**
 * UIDGenerator creates a generator (Supplier) to generate and return a new UID for each 'get' call.
 *
 * @author darryl.west@ebay.com
 */

import java.util.*;
import java.util.function.*;
import java.io.*;

public class UIDGenerator {

    public UID get() {
        return UID.createNew();
    }

    public static void main(String... args) {
        List<UID> list = new ArrayList<>();
        UIDGenerator gen = new UIDGenerator();
        Consumer<UID> consumer = UID::show;

        for (int i = 0; i < 5; i++) {
            UID uid = gen.get();
            System.out.println(uid);
            list.add(uid.update());
        }

        list.stream().forEach(uid -> consumer.accept(uid));
    }
}
