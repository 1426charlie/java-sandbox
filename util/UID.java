/**
 * UID is a data model
 *
 * @author darryl.west@ebay.com
 */

import java.util.*;
import java.util.function.*;
import java.io.*;

public class UID {
    protected String id;
    protected long dateCreated;
    protected long lastUpdated;
    protected long version;

    private UID() { }

    public static UID createNew() {
        UID uid = new UID();
        uid.id = UUID.randomUUID().toString().replace("-", "");
        uid.dateCreated = System.currentTimeMillis();
        uid.lastUpdated = System.currentTimeMillis();
        uid.version = 0;

        return uid;
    }

    public UID update() {
        UID uid = new UID();
        uid.id = this.id;
        uid.dateCreated = this.dateCreated;
        uid.lastUpdated = System.currentTimeMillis();
        uid.version++;

        return uid;
    }

    public String toString() {
        return String.format("id:%s,dateCreated:%d,lastUpdated:%d,version:%d", id, dateCreated, lastUpdated, version);
    }

    // create as a consumer...
    public static void show(UID uid) {
        System.out.println(uid);
    }
}

