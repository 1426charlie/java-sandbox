/**
 * Demonstrate integer iterator and generator streams ; use function objects to consume stream data
 *
 * @author darryl.west@ebay.com
 */

import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import java.io.*;

public class NumberStream {
    static abstract class Burst {
        Consumer<Integer> consumer;
        public Burst(Consumer<Integer> consumer) {
            this.consumer = consumer;
        }

        void newLine() {
            System.out.println("");
        }

        abstract void go(int start, int count);
    }

    static class ShortBurst extends Burst {
        public ShortBurst(Consumer<Integer> consumer) {
            super(consumer);
        }

        void go(int start, int count) {
            IntStream.iterate(start, n -> n + 1)
                .limit(count)
                .forEach(n -> consumer.accept(n));

            newLine();
        }
    };

    static class RandomBurst extends Burst {
        Random random = new Random();
        public RandomBurst(Consumer<Integer> consumer) {
            super(consumer);
        }

        void go(int start, int count) {
            // return only positive integers...
            IntStream.iterate(random.nextInt(Integer.MAX_VALUE), n -> random.nextInt(Integer.MAX_VALUE))
                .limit(count)
                .forEach(n -> consumer.accept(n));

            newLine();
        }
    }

    static class SortedRandomBurst extends Burst {
        Random random = new Random();
        public SortedRandomBurst(Consumer<Integer> consumer) {
            super(consumer);
        }

        void go(int start, int count) {
            List<Integer> list = new ArrayList<>();

            IntStream.iterate(random.nextInt(Integer.MAX_VALUE), n -> random.nextInt(Integer.MAX_VALUE))
                .limit(count)
                .forEach(n -> list.add(n));

            list.stream()
                .sorted()
                .forEach(n -> consumer.accept(n));
            newLine();
        }
    }

    static class IDGenerator {
        int radix = 34;
        Random random = new Random();
        Supplier<String> supplier = () -> Long.toString(random.nextInt(Integer.MAX_VALUE), radix);

        String nextID() {
            String ts = Long.toString(System.currentTimeMillis(), radix);
            StringBuilder sb = new StringBuilder().append(ts);

            Stream.generate(supplier)
                .limit(5)
                .forEach(s -> {
                    sb.append(s);
                });

            return sb.toString().substring(0,32);
        }
    }

    public static void main(String... args) {
        Console console = System.console();

        Consumer<Integer> func = (n -> System.console().printf("%d ", n));
        new NumberStream.ShortBurst(func).go(10, 20);
        new NumberStream.RandomBurst(func).go(0, 8);

        for (int i = 0; i < 5; i++) {
            String id = new NumberStream.IDGenerator().nextID();
            console.printf("id=%s, length(%d)\n", id, id.length());
        }

        new NumberStream.SortedRandomBurst(func).go(0, 20);
    }
}
