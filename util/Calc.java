/**
 * This is a good example of how to use a hashmap and lambdas to replace a classic switch/case statement. The hashmap
 * is easier to test and expand with new operations.
 *
 * @author darryl.west@ebay.com
 */

import java.util.*;
import java.util.function.*;
import java.io.*;

public class Calc {
    public static void main(String... args) {
        Console console = System.console();

        Map<String, IntBinaryOperator> ops = new HashMap<>();
        ops.put("add", Integer::sum);
        ops.put("sub", (a, b) -> a - b);
        ops.put("mul", (a, b) -> a * b);
        ops.put("div", (a, b) -> a / b);

        int a = 7;
        int b = 3;
        console.printf("%d + %d = %d\n", a, b, ops.get("add").applyAsInt(a, b));
        console.printf("%d - %d = %d\n", a, b, ops.get("sub").applyAsInt(a, b));
        console.printf("%d * %d = %d\n", a, b, ops.get("mul").applyAsInt(a, b));
        console.printf("%d / %d = %d\n", a, b, ops.get("div").applyAsInt(a, b));
    }
}
