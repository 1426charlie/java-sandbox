/**
 * The country model defines a name, continent and population for the country.  Using lambdas, write a function to return the total
 * population of countries for a given continent.
 *
 * The implementation uses streams and aggregates to maximize parallel operations.  An alternate method uses map/filter/reduce to get the sum.
 */

import java.util.*;
import java.util.function.*;
import java.io.*;

public class Country {
    Console console = System.console();

    String name;
    int population;
    String continent;

    public Country(String name, int population, String continent) {
        this.name = name;
        this.population = population;
        this.continent = continent;
    }

    public String getName() {
        return name;
    }

    public int getPopulation() {
        return population;
    }

    public String getContinent() {
        return continent;
    }

    public String showTotal() {
        return String.format("%s population %d million\n", name, population);
    }

    static class Fn {
        String showTotals(final List<Country> countries, final String target) {
            StringBuilder sb = new StringBuilder();
            countries
                .stream()
                .filter(c -> c.getContinent()  == target)
                .forEach(c -> sb.append(c.showTotal()));

            return sb.toString();
        }

        int calcSum(final List<Country> countries, final String target) {
            return countries
                .stream()
                .filter(c -> c.getContinent() == target)
                .mapToInt(Country::getPopulation)
                .sum();
        }

        int reduceSum(final List<Country> countries, final String target) {
            return countries
                .stream()
                .filter(c -> c.getContinent() == target)
                .mapToInt(Country::getPopulation)
                .reduce(0, (a, b) -> a + b);
        }
    }

    public static void main(String... args) {
        Console console = System.console();
        List<Country> countries = new ArrayList<>();

        // setup data
        countries.add(new Country("United States", 320, "North America"));
        countries.add(new Country("Canada", 30, "North America"));
        countries.add(new Country("Mexico", 45, "North America"));

        countries.add(new Country("Libia", 47, "Africa"));
        countries.add(new Country("Botzwanna", 17, "Africa"));
        countries.add(new Country("Ivory Coast", 37, "Africa"));

        // create the func handlers
        Country.Fn fn = new Country.Fn();

        // find for a target continent
        String target = "Africa";
        console.printf("%s\n", fn.showTotals(countries, target));
        int total = fn.calcSum(countries, target);

        console.printf("Total population of %s is %d million...\n\n", target, total);

        target = "North America";
        console.printf("%s\n", fn.showTotals(countries, target));
        total = fn.reduceSum(countries, target);
        console.printf("Total population of %s is %d million...\n\n", target, total);
    }
}
