import java.util.*;
import java.io.*;

class SeededRand {
  public static void main(String... args) {
    System.out.println("Hello world!");
    Random r1 = new Random(2243l);
    Random r2 = new Random(2243l);

    for (int i = 0; i < 100; i++) {
      Double d1 = r1.nextDouble() * 1000;
      Double d2 = r2.nextDouble() * 1000;
      System.out.printf("%b %2f %2f\n", d1 - d2 == 0.0, d1, d2);
    }
  }
}
