/**
 * demonstrates how a source thread can communicate with a sink thread.
 *
 * Use Case: thread 1 is a worker thread that needs to report it's output to a receiver thread;
 */

import java.io.*;
import java.util.Date;

public class ThreadedPipes {
    class SourceThread implements Runnable {
        final PipedOutputStream output = new PipedOutputStream();

        @Override
        public void run() {
            System.out.println("worker ready...");
            try {
                int i = 0;
                while (i < 4) {
                    String msg = String.format("working @ %d\n", new Date().getTime());
                    output.write(msg.getBytes());

                    Thread.sleep(1000);
                    i++;
                }
                output.flush();
                output.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        PipedOutputStream getOutput() {
            return output;
        }
    }

    class SinkThread implements Runnable {
        final PipedInputStream input = new PipedInputStream();

        void connect(PipedOutputStream out) throws IOException {
            input.connect(out);
        }

        @Override
        public void run() {
            System.out.println("waiting for worker to start...");
            try {
                int data = input.read();
                while (data != -1) {
                    System.out.print((char) data);
                    data = input.read();
                }
                System.out.println("worker complete...");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public SourceThread createSource() {
        return new SourceThread();
    }

    public SinkThread createSink() {
        return new SinkThread();
    }

    public static void main (String[] args) throws Exception {
        ThreadedPipes tp = new ThreadedPipes();

        SourceThread source = tp.createSource();
        SinkThread sink = tp.createSink();

        sink.connect(source.getOutput());

        new Thread(sink).start();
        Thread.sleep(3000);
        new Thread(source).start();
    }
}
