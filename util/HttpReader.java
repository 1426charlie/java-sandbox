
import java.io.*;
import java.net.*;
import java.util.*;

public class HttpReader {
    public static void main(String... args) throws Exception {
        URL url = new URL("http://raincity.s3-website-us-east-1.amazonaws.com/config/links.json");
        HttpURLConnection uc = (HttpURLConnection)url.openConnection();
        uc.connect();

        String line;
        StringBuilder buf = new StringBuilder();
        BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
        while ((line = in.readLine()) != null) {
            buf.append(line + "\n");
        }

        System.out.println(buf.toString());
    }
}
