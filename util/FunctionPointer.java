/**
 * FunctionPointer : demonstrates how to use Callable, Supplier, Consumer and Function to create swappable function pointers
 * to ease testing.
 *
 * @author darryl.west
 * @created 2018-12-23 11:50:35
 */

import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import java.util.concurrent.*;
import java.net.*;

public class FunctionPointer {
    Consumer<String> log = (str) -> {
        System.out.printf("%d [%s] %s\n", System.currentTimeMillis(), Thread.currentThread().getName(), str);
    };

    // provide a public interface and send to consumer
    public void log(String format, Object... args) {
        log.accept(String.format(format, args));
    }

    class Site {
        String name;
        String suri;
        public Site(String name, String suri) {
            this.name = name;
            this.suri = suri;
        }
    }

    // the jdk7 way...
    Callable<String> greeting = new Callable<String>() {
        public String call() throws Exception {
            return "this is the original reference string";
        }
    };

    // create and fill the map with know sites...
    Supplier<List<Site>> scanList = () -> {
        return Arrays.asList(
            new Site("google", "https://google.com"),
            new Site("ebay", "https://ebay.com"),
            new Site("docker", "https://hub.docker.com"),
            new Site("bigO", "https://bigocheatsheet.com"),
            new Site("oracle", "https://oracle.com"),
            new Site("elmwoodshoes", "https://elmwoodvillageshoes.com")
        );
    };

    Function<URI, String> scanSite = (uri) -> {
        // create a webtarget client
        // send the request
        String resp = String.format("%s : %s", uri, "my response...");
        try {
            // simulate some work...
            Thread.sleep(new Random().nextInt(100));
        } catch (Exception ignore) {}


        return resp;
    };

    public Function<Stream<Site>, List<String>> scanAll = (sites) -> {
        List<String> results = new ArrayList<>();
        sites.forEach(site -> {
            URI uri = null;
            log.accept("scan " + site.suri);
            uri = URI.create(site.suri);
            results.add(scanSite.apply(uri));
        });

        return results;
    };

    public static void main(String... args) throws Exception {
        FunctionPointer fp = new FunctionPointer();

        fp.log("Test 1 : swap the greeting function...");
        fp.log(fp.greeting.call());

        // create a mock on the fly...
        fp.greeting = new Callable<String>() {
            public String call() throws Exception {
                return "this is the new mock greeting";
            }
        };

        fp.log(fp.greeting.call());

        // test 2
        fp.log("Test 2 : swap the scan site function...");

        List<Site> sites = fp.scanList.get();

        // replace with streams to enable running in parallel
        List<String> results = fp.scanAll.apply(sites.stream());
        fp.log("results: %s", results);

        fp.scanSite = (uri) -> {
            return String.format("warning! %s not reachable!", uri);
        };

        fp.log("results: %s", fp.scanAll.apply(sites.parallelStream()));
    }
}
