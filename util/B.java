/**
 * extend and implement the abstract base class
 */

import java.util.*;
import java.io.*;

public class B extends A {
    Console log = System.console();

    public B(String name) {
        super(name);
        log.printf("my name %s\n", name);
    }

    public void run() {
        log.printf("log statement from run() of B %s\n", name);
    }

    public String tester(String param) {
        String s = super.tester(param);
        log.printf("my a tester: %s\n", s);
        return String.format("b:%s", s);
    }

    public static void main(String... args) {
        B b = new B("freddy");

        b.run();
        
        String p = b.tester("my-param");
        b.log.printf("my b tester: %s\n", p);
    }
}

