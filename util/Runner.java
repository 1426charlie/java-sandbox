/**
 * An example of how to runnable in a functional way.
 *
 * @author darryl.west@ebay.com
 */

import java.util.*;
import java.util.function.*;
import java.io.*;

public class Runner {
    Console console = System.console();

    Runnable worker;
    public Runner(Runnable worker) {
        this.worker = worker;
    }

    public void go() {
        console.printf("I'm working!\n");
        worker.run();
        console.printf("Done working...\n");
    }

    public static void main(String... args) {
        Console console = System.console();
        Runner workRunner = new Runner( () -> {
            console.printf("<");
            for (int i = 0; i < 100; i++) {
                try {
                    Thread.sleep(20);
                } catch (Exception ignore) { }
                if (i % 10 == 0) {
                    console.printf("|");
                } else {
                    console.printf(".");
                }
            }
            console.printf("|>\n");
        });

        Runnable runner = () -> {
            console.printf("i'm running...\n");
            workRunner.go();
            console.printf("completed....\n");
        };

        runner.run();
    }
}
