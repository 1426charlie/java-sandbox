/**
 * Recursion examples
 *
 * @author darryl.west@ebay.com
 */

import java.util.*;
import java.util.function.*;
import java.io.*;

public class Recursions {
    Console console = System.console();

    // reverse the phrase
    public void head(String phrase) {
        if (phrase.length() == 0) {
            return;
        }

        head(phrase.substring(1));
        console.printf("%s", phrase.charAt(0));
    }

    // echo the phrase
    public void tail(String phrase) {
        if (phrase.length() == 0) {
            return;
        }

        console.printf("%s", phrase.charAt(0));
        tail(phrase.substring(1));
    }

    public static void main(String... args) {
        Recursions r = new Recursions();
        r.head("\nmy phrase");
        r.tail("another phrase\n");
    }
}

