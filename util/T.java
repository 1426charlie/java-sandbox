
class T {
    public static void main(String... args) {
        int maxcount = 2;
        String smax = System.getenv("DEV_MAX_COUNT");

        if (smax != null) {
            try {
                maxcount = Integer.parseInt(smax);
            } catch (Exception ignore) {
                System.err.println("could not convert DEV_MAX_COUNT " + smax + " to int...");
            }
        }

        System.out.println("max count = " + maxcount);
    }
}
