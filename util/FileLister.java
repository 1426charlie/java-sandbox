import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.atomic.AtomicBoolean;

public class FileLister {
    String root;

    FileLister(String root) {
        this.root = root;
    }

    boolean isBuildRequired(long lastBuild) throws Exception {
        final AtomicBoolean buildRequired = new AtomicBoolean(false);

        Files.find(Paths.get(root), Integer.MAX_VALUE,
                (filePath, fileAttr) -> fileAttr.lastModifiedTime().toMillis() > lastBuild).forEach(fp -> {
                    if (fp.getFileName().toString().endsWith(".java")) {
                        buildRequired.lazySet(true);
                    }
                });

        return buildRequired.get();
    }

    void start() throws Exception {
        long lastBuild = 0l;
        for (;;) {
            if (isBuildRequired(lastBuild)) {
                lastBuild = build();
                log("last build: %d", lastBuild);
            }

            Thread.sleep(500);
        }
    }

    // return the last build time in millis
    long build() throws Exception {
        log("build required");

        Thread.sleep(5000);

        return System.currentTimeMillis();
    }

    void log(String format, Object... args) {
        String msg = String.format(format, args);
        System.out.printf("%d %s %s\n", System.currentTimeMillis(), Thread.currentThread().getName(), msg);
    }

    public static void main(String... args) throws Exception {
        FileLister lister = new FileLister("/Users/darwest/visibility-hacking/socialshare/src");
        lister.start();
    }
}