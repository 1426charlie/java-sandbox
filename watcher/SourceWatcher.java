
/**
 * SourceWatcher - watch standard java project and execute build/test on any file changes.
 * 
 * @author darryl.west
 * @created 2019-01-02 05:39:14
 */

import java.io.*;
import java.nio.file.*;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.cli.*;

public class SourceWatcher {
    public final static String VERSION = "19.1.2d";

    long loopInterval = 1000l;
    List<String> commandList = Arrays.asList("make", "test");

    AtomicBoolean processRunning = new AtomicBoolean(false);
    String root;

    public SourceWatcher(String root) {
        this.root = root;
    }

    // poll the files to find recent changes
    boolean isBuildRequired(long lastBuild) throws Exception {
        final AtomicBoolean buildRequired = new AtomicBoolean(false);

        Files.find(Paths.get(root), Integer.MAX_VALUE,
                (filePath, fileAttr) -> fileAttr.lastModifiedTime().toMillis() > lastBuild).forEach(fp -> {
                    if (fp.getFileName().toString().endsWith(".java")) {
                        buildRequired.lazySet(true);
                    }
                });

        return buildRequired.get();
    }

    // start the watcher loop
    void start() throws Exception {
        long lastBuild = System.currentTimeMillis();
        for (;;) {
            if (isBuildRequired(lastBuild)) {
                lastBuild = build();
                log("last build: %s", Instant.ofEpochMilli(lastBuild).toString());
            }

            Thread.sleep(loopInterval);
        }
    }

    // return the last build time in millis
    long build() throws Exception {
        log("build required");

        exec();

        return System.currentTimeMillis();
    }

    // run the build
    void exec() throws Exception {
        if (processRunning.getAndSet(true)) {
            return;
        }

        log("run the process...");

        try {
            // do the external call
            Process process = new ProcessBuilder(commandList).start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;

            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
        } finally {
            processRunning.set(false);
        }
    }

    void parseArgs(String[] args) throws Exception {
        Options options = new Options();

        options.addOption("h", "help", false, "show this help");
        options.addOption("?", false, "show this help");
        options.addOption("f", "folder", true, "set the watch folder, defaults to 'src'");
        options.addOption("i", "interval", true, "set the loop interval in millis, defaults to 1000");
        options.addOption("verbose", false, "set verbose mode");
        options.addOption("v", "version", false, "show the version");

        CommandLine line = new DefaultParser().parse(options, args);

        if (line.hasOption("help") || line.hasOption("?")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("watcher", options);
            showVersion();
            System.exit(-1);
        }

        if (line.hasOption("version")) {
            showVersion();
        }

        if (line.hasOption("interval")) {
            loopInterval = new Long(line.getOptionValue("interval")).longValue();
            log("loop interval set to %d\n", loopInterval);
        }

        if (line.hasOption("folder")) {
            root = line.getOptionValue("folder");
        }
    }

    void showVersion() {
        System.out.printf("Version: %s\n", VERSION);
    }

    void log(String format, Object... args) {
        String msg = String.format(format, args);
        System.out.printf("%d [%s] %s\n", System.currentTimeMillis(), Thread.currentThread().getName(), msg);
    }

    public static void main(String[] args) throws Exception {
        final SourceWatcher watcher = new SourceWatcher("src/");
        watcher.parseArgs(args);

        watcher.log("Start watcher version %s for source folder: %s", watcher.VERSION, watcher.root);
        watcher.start();
    }
}
