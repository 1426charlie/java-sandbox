/**
 *
 * This class is based on sun.net server to enable a very small footprint.  The intent is
 * to provide a fast-loading service to support unit tests that replace mocks to provide
 * more complete tets.
 *
 * @see https://docs.oracle.com/javase/8/docs/jre/api/net/httpserver/spec/index.html?com/sun/net/httpserver/HttpServer.html
 *
 */

import java.io.*;
import java.net.*;
import java.time.*;

import com.sun.net.httpserver.*;

public class ServiceMock {
    protected int port = 8000;

    public void start() throws Exception {
        log("creating server at port %d", port);
        HttpServer server = createServer(port);

        createRoutes(server);

        server.setExecutor(null); // creates a default executor

        log("starting the server...");

        server.start();
    }

    // override this for special cases
    protected HttpServer createServer(int port) throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);

        return server;
    }

    // override this to add custom routes
    protected void createRoutes(HttpServer server) {
        // default handlers
        server.createContext("/hello", new HelloHandler());
        server.createContext("/shutdown", new ShutdownHandler(server));
    }

    class HelloHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            log("method: %s %s", t.getRequestMethod(), t.getRequestURI());

            String response = String.format("{\"%s\":\"%s\"}\n", "current-time", Instant.now().toString());
            writeResponse(t, response);
        }
    }

    class ShutdownHandler implements HttpHandler {
        HttpServer server;

        ShutdownHandler(HttpServer server) {
            this.server = server;
        }

        @Override
        public void handle(HttpExchange t) throws IOException {
            String response = "shutting down...\n";
            t.getResponseHeaders().set("Content-Type", "text/plain");
            writeResponse(t, response);

            try {
                log("shutting down...");
                Thread.sleep(250);
                server.stop(0);
            } catch(InterruptedException ex) {
                log("%s", ex.getMessage());
            }
        }
    }

    void writeResponse(HttpExchange t, String response) throws IOException {
        this.writeResponse(t, response, 200);
    }

    void writeResponse(HttpExchange t, String response, int code) throws IOException {
        // set the content type then the status code and length; a bug in HttpExchange requires this
        Headers headers = t.getResponseHeaders();
        if (headers.get("Content-Type") == null) {
            // TODO: is this response really json?
            headers.set("Content-Type", "application/json");
        }

        t.sendResponseHeaders(code, response.length());

        OutputStream os = t.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

    void log(String format, Object... args) {
        System.out.printf("%d INFO %s\n", System.currentTimeMillis(), String.format(format, args));
    }

    public static void main(String[] args) throws Exception {
        ServiceMock server = new ServiceMock();
        System.out.printf("Service starting at %s\n", Instant.now().toString());
        server.start();
    }
}
