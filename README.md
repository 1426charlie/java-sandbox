# Java Sandbox

## REPLs

* [jshell jdk9](https://docs.oracle.com/javase/9/jshell/), [jdk9 container image](https://hub.docker.com/r/darrylwest/alpine-sdk9/)
* [java, on-line](http://www.javarepl.com/term.html)
* [groovysh, local](http://groovy-lang.org/)

## Compiler / Watcher

_Add a compiler and watcher with gradle_

###### darryl.west | 2018-03-29

