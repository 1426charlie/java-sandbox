package functional;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.*;
import java.io.*;

public class Looping {
	// define a UnaryOperator<T>
	public Function<Integer, Integer> squareFunction = x -> x*x;
	
	// generic typed 
	public static class Stringer<T> implements Consumer<T> {
		StringBuilder buf = new StringBuilder();
		
		@Override
		public void accept(T t) {
			if (buf.length() > 0) {
				buf.append(",");
			}
			buf.append(t);
		}
		
		public Stringer<T> add(T t) {
			this.accept(t);
			return this;
		}
		
		@Override
		public String toString() {
			return buf.toString();
		}
	}
	
	// will show the list in any order (parallel)
	void showIntList(PrintStream writer, Integer...ints) {
		if (writer == null) {
			writer = System.out;
		}
		
		Consumer<? super Integer> consumer = (Consumer<? super Integer>) new Stringer<Integer>();
		Stream<Integer> stream = Stream.of(ints).parallel();
		stream.forEach(consumer);
		
		writer.println(consumer);
	}
	
	int sum(int arr[]) {
		return Arrays.stream(arr).parallel().sum();
	}
	
	int sum(List<Integer> list) {
		return list.stream().mapToInt(x -> x.intValue()).parallel().sum();
	}
	
	public static void main(String... args) {
		Looping looping = new Looping();
		
		looping.showIntList(null, 1,2,3,4,5);
		
		
		System.out.println(looping.squareFunction.apply(5));
	}
}
