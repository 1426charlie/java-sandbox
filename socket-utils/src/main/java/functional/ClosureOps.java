package functional;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class ClosureOps {
	int instanceLength;
	
	public Function<String,String> getStringOperation() {
		return target -> {
			int localLength = target.length();
			instanceLength = target.length();
			return String.format("%s:%d:%d", target.toLowerCase(), instanceLength, localLength);
		};
	}
	
	public Predicate<Integer> tooLarge = n -> n > 100;
	
	public static void main(String[] args) {
		ClosureOps ops = new ClosureOps();
		
		final Function<String,String> fn = ops.getStringOperation();
		String s = fn.apply("this is my string");
		System.out.println(s);
		
		List<Integer> list = Arrays.asList(230, 45, 13, 533, 4);
		Stream<Integer> stream = list.parallelStream();
		stream.forEach(n -> System.out.println(String.format("%d too big %b", n, ops.tooLarge.test(n))));
	}

}
