package sockets;

import java.io.*;
import java.util.*;
import java.net.*;

public class TCPClient {
	final static String VERSION = "1.0.1";
    Console log = System.console();
	
	String[] commands = {
		"version", "status", "ping", "ping",
		"status", "ping", "ping",
		"status", "version", "ping",
		"fetch/01C5H7XNBPGC73TAFX9JXPKX0F",
		"close" 
	};
	
	String sendRequest(String cmd, BufferedReader reader, PrintWriter writer) {
		try {
			writer.println(cmd);
			
			String response = reader.readLine();
			
			return response;
		} catch (Exception ex) {
			return "service down";
		}
	}
	
	public void start(String host, int port) {
		Socket socket = null;
		
		List<String> list = Arrays.asList(commands);
		
		try {
			log.printf("listen on %s:%d\n", host, port);
			socket = new Socket(host, port);
			socket.setSoTimeout(10000);
			socket.setTcpNoDelay(true);
			
			// write requests
			PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);

			// wait for responses
			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			for (String cmd : list) {
                log.printf("request: %-8s -> ", cmd);
				String response = sendRequest(cmd, reader, writer);
				log.printf("%s\n", response);
				
				try {
					Thread.sleep(500);
				} catch (Exception ignore) { };
			}
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException ignore) { }
		}
	}
	
	public static void main(String[] args) {
		TCPClient client = new TCPClient();
		String host = "127.0.0.1";
		
		client.start(host, 9090);
	}
}
