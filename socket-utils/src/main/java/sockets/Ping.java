package sockets;

import java.net.*;
import java.io.*;

public class Ping {
	
	public boolean knownHost(String name) {
		try {
			InetAddress host = InetAddress.getByName(name);
			return true;
		} catch (UnknownHostException e) {
			return false;
		}
	}
	
	public boolean isReachable(String host, int count) {
		try {
			Process proc = Runtime.getRuntime().exec("ping -c 3 " + host);
			BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			String line = null;
			int packets = count;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
				
				// should parse the response to check for n packets transmitted, n packets received, 0.0% packet loss...
			}
			
			return count == packets;
		} catch (Exception e) {
			return false;
		}
		
	}
	
	public static void main(String[] args) {
		Ping ping = new Ping();

		String hostName = "raincitysoftware.com";
		if (!ping.knownHost(hostName)) {
			System.out.println(hostName + " is not a valid host...");
			return;
		}
		
		if (!ping.isReachable(hostName, 3)) {
			System.out.println(hostName = " is not reachable...");
		}

	}
}
