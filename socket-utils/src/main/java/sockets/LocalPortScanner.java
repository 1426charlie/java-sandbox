package sockets;

import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

public class LocalPortScanner {
    final static String VERSION = "1.0.0";
    // final static Logger log = Logger.getLogger(LocalPortScanner.class);

    public boolean isPortOpen(int port) {
    		try {
    			ServerSocket ss = new ServerSocket(port);
    			System.out.println(String.format("port %d is open: ", port));
    			ss.close();
    			return true;
    		} catch (Exception e) {
    			System.out.println(String.format("port %d is closed: ", port));
    			return false;
    		}
    }

	public List<String> scan(int start, int end) {
		List<String> list = new ArrayList<String>();
		
		for (int i = start; i <= end; i++) {
			if (!isPortOpen(i)) {
				list.add(String.format("port %d in use...", i));
			}
		}
		
		return list;
	}
	
    public static void main(String[] args) throws Exception {
        System.out.println("Local Port Scanner Version: " + VERSION);
        
        LocalPortScanner scanner = new LocalPortScanner();
        List<String> list = scanner.scan(8078, 8082);
        
        System.out.println(list);

        System.out.println("complete...");
    }
}
