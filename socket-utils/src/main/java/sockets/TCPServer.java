package sockets;

import java.util.*;
import java.util.function.*;
import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class TCPServer {
	final static String VERSION = "1.0.1";
    Console log = System.console();

    // use a base 36 string created from nano-seconds for client id
    Supplier<String> uniqueIDGenerator = () -> Long.toString(System.nanoTime(), 36).toUpperCase();

    Function<PrintWriter, Boolean> closeHandler = out -> {
        out.println("over and out...");
        return true;
    };

    Function<PrintWriter, Boolean> pingHandler = out -> {
        out.println("pong");
        return false;
    };

    Function<PrintWriter, Boolean> versionHandler = out -> {
        out.println(VERSION);
        return false;
    };

    Function<PrintWriter, Boolean> createStatusHandler(String id) {
        Function<PrintWriter, Boolean> fn = out -> {
            out.println("client id:" + id + ", all ok...");
            return false;
        };

        return fn;
    }
	
	class Client implements Runnable {
        Map<String, Function<PrintWriter, Boolean>> ops = new HashMap<>();

		private Socket sock = null;
		private String id;

		Client(Socket socket, String id) {
			this.sock = socket;
			this.id = id;

            ops.put("close", closeHandler);
            ops.put("ping", pingHandler);
            ops.put("version", versionHandler);

            ops.put("status", createStatusHandler(id));
		}
		
		public String getId() {
			return id;
		}

		@Override
		public void run() {
			
			try {
				// TODO add timeouts
				PrintWriter out = new PrintWriter(sock.getOutputStream(), true);
				
				BufferedReader input = new BufferedReader(new InputStreamReader(sock.getInputStream()));
				boolean done = false;
				while (!done) {
					try {
						String line = input.readLine();
						log.printf("client %s request -> %s\n", id, line);

                        Function<PrintWriter, Boolean> op = ops.get(line);
                        if (op != null) {
                            done = op.apply(out);
						} else {
							out.println(id + ":<:" + line);
						}
					} catch (SocketTimeoutException ex) {
						log.printf("client id: %s timed-out, exiting...", id);
                        out.println("id:" + id + "timout reached, closing connection...");
						done = true;
					}
				}
				
				input.close();
				out.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				log.printf("client %s connection closed...\n", id);
				try {
					sock.close();
				} catch (IOException ignore) { }
			}
		}
	}

	public void start(int port) throws Exception {
		log.printf("start socket server on port %d...\n", port);
		ServerSocket ss = null;
        int timeout = 10000;

		try {
			ss = new ServerSocket(port);
			log.printf("listen for connections on port: %d\n", port);
			while (true) {
				Socket sock = ss.accept();

				String id = uniqueIDGenerator.get();

				sock.setSoTimeout(timeout);
				log.printf("New client connected, id: %s, timeout: %d\n", id, timeout);
				
				Client client = new Client(sock, id);
				// add the client to the list
				new Thread(client).start();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			ss.close();
		}
	}
	
	public static void main(String[] args) throws Exception {
		TCPServer server = new TCPServer();
		
		server.start(9090);
	}
}
