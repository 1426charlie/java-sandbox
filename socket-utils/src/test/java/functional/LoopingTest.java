package functional;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class LoopingTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSum() {
		Looping lp = new Looping();
		int arr[] = {5, 3, 44, 21};
		 
		int expected = Arrays.stream(arr).reduce(0, (x, y) -> x + y);
		
		int sum = lp.sum(arr);
		
		assertEquals("sum up", expected, sum);
		
		List<Integer> list = new ArrayList<Integer>();
		list.add(44);
		list.add(33);
		list.add(11);
		
		sum = lp.sum(list);
		assertEquals("list sum", 88, sum);
	}
	
	@Test
	public void testStringer() {
		Looping.Stringer<String> stringer = new Looping.Stringer<String>();
		stringer.add("mary").add("tom").add("dick").add("harry");
		String names = stringer.toString();
		assertEquals("names", "mary,tom,dick,harry", names);
	}

}
