package functional;

import static org.junit.Assert.*;

import java.util.function.Function;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ClosureOpsTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testStringOperation() {
		ClosureOps ops = new ClosureOps();
		final Function<String,String> fn = ops.getStringOperation();
		String s = fn.apply("my string");
		assertEquals("length", "my string:9:9", s);
	}

    @Test
    public void testLargeInt() {
		ClosureOps ops = new ClosureOps();
		boolean f = ops.tooLarge.test(5);
		assertEquals("small", f, false);
		f = ops.tooLarge.test(101);
		assertEquals("too large", f, true);
    }
}
