/**
 * 
 */
package sockets;

import static org.junit.Assert.*;

import java.net.ServerSocket;
import java.util.List;
import java.util.Random;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author dpw
 *
 */
public class LocalPortScannerTest {

    static int port;
    static ServerSocket ss;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass public static void setUp() throws Exception {
        // start listening on the designated port
        port = new Random().nextInt(500) + 26700;
        ss = new ServerSocket(port);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass public static void tearDown() throws Exception {
        ss.close();
	}

	@Test public void testList() {
		LocalPortScanner scanner = new LocalPortScanner();
		List<String> list = scanner.scan(port, port);
		assertEquals("list should not be empty", 1, list.size());
	}

}
