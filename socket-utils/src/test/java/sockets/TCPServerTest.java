package sockets;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.io.*;
import java.util.function.*;

// import org.slf4j.*;

public class TCPServerTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testStart() {
        TCPServer server = new TCPServer();
        assertNotNull("server instance", server);
	}

	@Test
	public void testCloseHandler() {
        StringWriter sw = new StringWriter();
		
        TCPServer server = new TCPServer();
        boolean f = server.closeHandler.apply(new PrintWriter(sw));
        assertTrue("flag", f);
        assertEquals("over and out...\n", sw.toString());
    }

	@Test
	public void testPingHandler() {
        StringWriter sw = new StringWriter();
		
        TCPServer server = new TCPServer();
        boolean f = server.pingHandler.apply(new PrintWriter(sw));
        assertFalse("flag", f);
        assertEquals("pong\n", sw.toString());
    }

	@Test
	public void testVersionHandler() {
        StringWriter sw = new StringWriter();
		
        TCPServer server = new TCPServer();
        boolean f = server.versionHandler.apply(new PrintWriter(sw));
        assertFalse("flag", f);
        assertEquals("1.0.1\n", sw.toString());
    }

    @Test
    public void testStatusHandler() {
        StringWriter sw = new StringWriter();
		
        TCPServer server = new TCPServer();
        boolean f = server.createStatusHandler("myid").apply(new PrintWriter(sw));
        assertFalse("flag", f);
        assertEquals("client id:myid, all ok...\n", sw.toString());
    }
}
